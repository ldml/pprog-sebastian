#include "nvector.h"
#include "stdio.h"
#include "stdlib.h"

int main(){

	double n=3;
	printf("\ntesting nvector_alloc ...\n");
	nvector *v = nvector_alloc(n);
	if (v == NULL) printf("test failed\n");
	else printf("test passed\n");

	nvector *u = nvector_alloc(n);
	printf("\ntesting nvector_set and nvector_get...\n");

	double h[6];
	for(int i=0; i<6; i++){
	h[i]=(double)rand()/RAND_MAX;
	}
	printf("Two vectors v and u are created with the function nvector_set. v is set to (%g, %g, %g) and u is set to (%g, %g, %g) \n",h[0],h[1],h[2],h[3],h[4],h[5]);

	nvector_set(v,0,h[0]);
	nvector_set(v,1,h[1]);
	nvector_set(v,2,h[2]);
	nvector_set(u,0,h[3]);
	nvector_set(u,1,h[4]);
	nvector_set(u,2,h[5]);


	printf("The two vectors are checked with the nvector_get function:\n");
	printf("v = (%g,%g,%g)\n",nvector_get(v,0),nvector_get(v,1),nvector_get(v,2));
	printf("u = (%g,%g,%g)\n",nvector_get(u,0),nvector_get(u,1),nvector_get(u,2));

	if(nvector_get(v,0)==h[0] && nvector_get(v,1)==h[1] && nvector_get(v,2)==h[2]
&& nvector_get(u,0)==h[3] && nvector_get(u,1)==h[4] && nvector_get(u,2)==h[5]){printf("test passed\n");}
	else{printf("test failed\n");}


	printf("\ntesting nvector_dot_product...\n");
	double d = nvector_dot_product(v,u);
	printf("Dot product between v and u = %g\n",d);
	
	if(d==h[0]*h[3]+h[1]*h[4]+h[2]*h[5]){printf("test passed\n");}
	else{printf("test failed\n");}

	nvector_free(v);
	nvector_free(u);
	
	
return 0;
}
