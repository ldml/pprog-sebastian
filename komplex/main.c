#include<stdio.h>
#include"komplex.h"

int main(){

	double x=5, y=6;	

	printf("\ntesting komplex_set...\n");
	komplex z;
	komplex_set(&z,x,y);
	komplex Z={5,6};
	komplex_print("The complex number should be =", Z);
	komplex_print("The complex number actually is =",z);


	

	printf("\ntesting komplex_new...\n");
	komplex C = {1,6};
	komplex c = komplex_new(1,6);
	komplex_print("A new complex number should be =",C);
	komplex_print("The new complex number actually is =",c);
	



	komplex a = {1,2}, b = {3,4};

	printf("\ntesting komplex_add...\n");
	komplex r = komplex_add(a,b);
	komplex R = {4,6};
	komplex_print("a=",a);
	komplex_print("b=",b);
	komplex_print("a+b should   = ", R);
	komplex_print("a+b actually = ", r);





	printf("\ntesting komplex_sub...\n");
	komplex f = komplex_sub(a,b);
	komplex F = {-2,-2};
	komplex_print("a-b should =", F);
	komplex_print("a-b actually is =",f);


	

	printf("\ntesting komplex_conjugate...\n");
	komplex g = komplex_conjugate(r);
	komplex G = {4,-6};
	komplex_print("(a+b)* should =",G);
	komplex_print("(a+b)* actually =",g);

return 0;

}
