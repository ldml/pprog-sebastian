#include<stdio.h>
#include"komplex.h"

void komplex_print (char *s, komplex a) {
	printf ("%s (%g,%g)\n", s, a.re, a.im);
}

// Første:

void komplex_set(komplex* z, double x, double y){
	(*z).re = x;
	(*z).im = y;
	}

// Anden: 

komplex komplex_new(double x , double y){
komplex z={x,y};
return z;
}

// Tredje:

komplex komplex_add(komplex a, komplex b){
	double x = a.re + b.re, y = a.im + b.im;
	komplex result = {.re = x, .im = y};
	return result;
	}

// Fjerde:

komplex komplex_sub(komplex a, komplex b){
	double x = a.re - b.re, y = a.im - b.im;
	komplex result = {.re = x, .im = y};
	return result;
	}


// Femte:

komplex komplex_conjugate(komplex z){
	double i=-z.im, r=z.re;
	komplex result = {.re = r, .im = i};
	return result;
	}
