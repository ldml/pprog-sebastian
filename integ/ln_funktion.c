#include<math.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>

double ln_integrand(double x, void* params){
	return log(x)*pow(x,-0.5);
}

double ln_funktion(double a, double b){

	gsl_function f;
	f.function = ln_integrand;
	f.params = NULL;

	int limit = 100;
	double acc=1e-6,eps=1e-6,result,err;
	gsl_integration_workspace * workspace =
		gsl_integration_workspace_alloc(limit);
int status=gsl_integration_qag(&f,a,b,acc,eps,limit,1,workspace,&result,&err);
	gsl_integration_workspace_free(workspace);
	if(status!=GSL_SUCCESS) return NAN;
	else return result;
}
