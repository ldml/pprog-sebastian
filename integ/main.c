#include<stdio.h>
#include<math.h>

double ln_funktion(double,double);
double norm_funktion(double);
double hamil_funktion(double);

int main(){

	printf("Integrating the function ln(x)/sqrt(x) from 0 to 1 = %g\n\n\n",ln_funktion(0.0,1.0));


	printf("alpha \t energy\n");
for(double a=0.03;a<=3;a+=0.01)
	printf("%g \t %g \n",a,hamil_funktion(a)/norm_funktion(a));

return 0;
}
