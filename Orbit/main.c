#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int diff_eq(double t, const double y[], double dydt[], void * params){
	dydt[0]=y[0]*(1-y[0]);
return GSL_SUCCESS;
}

int diff_eqOrbit(double t, const double y[], double dydt[], void *params){
	double e = *(double *)params;
	dydt[0]=y[1];
	dydt[1]=1-y[0]+e*y[0]*y[0];
return GSL_SUCCESS;
}


double log_function(double x){
	gsl_odeiv2_system system;
	system.function = diff_eq;
	system.jacobian = NULL;
	system.dimension = 1;
	system.params = NULL;

double acc=1e-6,eps=1e-6,hstart=copysign(0.1,x);

gsl_odeiv2_driver* driver = gsl_odeiv2_driver_alloc_y_new(&system, gsl_odeiv2_step_rkf45, hstart, acc, eps);

double t=0,y[1]={0.5};

gsl_odeiv2_driver_apply(driver,&t,x,y);

gsl_odeiv2_driver_free(driver);

return y[0];
}


double Orbit_function(double phi, double e, double y0, double y1){
	gsl_odeiv2_system system;
	system.function = diff_eqOrbit;
	system.jacobian = NULL;
	system.dimension = 2;
	system.params = (void *) &e;


double acc=1e-6,eps=1e-6,hstart=copysign(0.1,phi);

gsl_odeiv2_driver* driver = gsl_odeiv2_driver_alloc_y_new(&system, gsl_odeiv2_step_rkf45, hstart, acc, eps);

double t=0,y[2]={y0, y1};

gsl_odeiv2_driver_apply(driver,&t,phi,y);

gsl_odeiv2_driver_free(driver);

return y[0];
}

int main(){

double flog(double x){
double value=1.0/(1.0+exp(-x));
return value;
}



for(double x=0;x<=3;x+=0.1){ printf("%g \t %g \t %g\n",x,log_function(x),flog(x));
}

printf("\n\n");

for(double phi=0;phi<=2*M_PI+0.1;phi+=0.1){ printf("%g \t %g\n",phi, Orbit_function(phi,0,1,0));
}

printf("\n\n");

for(double phi=0;phi<=2*M_PI+0.1;phi+=0.1){ printf("%g \t %g\n",phi, Orbit_function(phi,0,1,-0.5));
}

printf("\n\n");

for(double phi=0;phi<=6*M_PI+0.1;phi+=0.1){ printf("%g \t %g\n",phi, Orbit_function(phi,0.03,1,-0.5));
}


return 0;
}


