#include<stdio.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_math.h>
#include<gsl/gsl_eigen.h>
#include<gsl/gsl_linalg.h>

int equal(double a, double b, double tau); // function declaration

int main(){

// Opgave 1:

gsl_matrix *m = gsl_matrix_calloc(3, 3);


gsl_matrix_set(m, 0, 0, 6.13);
gsl_matrix_set(m, 0, 1, -2.90);
gsl_matrix_set(m, 0, 2, 5.86);
gsl_matrix_set(m, 1, 0, 8.08);
gsl_matrix_set(m, 1, 1, -6.31);
gsl_matrix_set(m, 1, 2, -3.89);
gsl_matrix_set(m, 2, 0, -4.36);
gsl_matrix_set(m, 2, 1, 1.00);
gsl_matrix_set(m, 2, 2, 0.19);

gsl_vector* xvector = gsl_vector_alloc(3);
gsl_vector* bvector = gsl_vector_alloc(3);

gsl_vector_set(bvector, 0, 6.23);
gsl_vector_set(bvector, 1, 5.37);
gsl_vector_set(bvector, 2, 2.29);

gsl_linalg_HH_solve(m,bvector,xvector);

// Her udskrives xvector fra opgave 1:
fprintf(stderr,"\n\nOpgave 1: \n x=(%g, %g, %g)\n\n",gsl_vector_get(xvector, 0),gsl_vector_get(xvector,1),gsl_vector_get(xvector, 2));

fprintf(stderr,"The result is now checked:");
double lign1=6.13*gsl_vector_get(xvector, 0)-2.90*gsl_vector_get(xvector, 1)+5.86*gsl_vector_get(xvector, 2);
double lign2=8.08*gsl_vector_get(xvector, 0)-6.31*gsl_vector_get(xvector, 1)-3.89*gsl_vector_get(xvector, 2);
double lign3=-4.36*gsl_vector_get(xvector, 0)+1.00*gsl_vector_get(xvector, 1)+0.19*gsl_vector_get(xvector, 2);

double tau=0.01;
if(equal(lign1,6.23,tau)==1 && equal(lign2,5.37,tau)==1 && equal(lign3,2.29,tau)==1){fprintf(stderr,"test passed\n");}
else{fprintf(stderr,"test failed\n");}

gsl_matrix_free(m);
gsl_vector_free(bvector);
gsl_vector_free(xvector);

// Opgave 2:

int n=200;
double s=1.0/(n+1);

// i)
gsl_matrix *H = gsl_matrix_calloc(n, n); // alloc. plads til nxn matrix. Matrix er fyldt med 0'er.

for(int x=0;x<n-1;x++){
gsl_matrix_set(H, x, x, -2); // saetter diagonalen lig -2.
gsl_matrix_set(H, x, x+1, 1); // saetter indgangene ved siden af diagonalen lig 1.
gsl_matrix_set(H, x+1, x, 1); // saetter indgangene under diagonalen lig 1.
}
gsl_matrix_set(H,n-1,n-1,-2); // indgang (n,n) i matricen er ikke blevet sat til -2 endnu, da denne ikke kan nås med forløkken ovenfor. Den sættes her.
gsl_matrix_scale(H,-1.0/pow(s,2)); // matricen skaleres med -1/s^2.



// ii)
gsl_eigen_symmv_workspace *w = gsl_eigen_symmv_alloc(n); // Her alloc. plads til at udregne egenværdierne og egenvektorene.

gsl_vector* evalue = gsl_vector_alloc(n); // Her alloc. plads til en vektor med n indgange, hvor egenvaerdierne indsættes.
gsl_matrix* evector = gsl_matrix_calloc(n,n); // Her alloc. plads til egenvektorene.
gsl_eigen_symmv(H,evalue,evector,w); // Her udregnes egenværdierne og egenvektorene og gemmes i hhv. evalue og evector.


// iii)
gsl_eigen_symmv_sort(evalue,evector,GSL_EIGEN_SORT_VAL_ASC); // Her sorteres egenværdierne og egenvektorene med lave vaerdier foerst.


// iv)
// Den eksakte løsning til den uendelige brønd er: E=n^2 hbar^2 pi^2 / (2mL^2), men i denne opgave regner vi med relative enheder, så energien epsilon = pi^2 n^2.
fprintf(stdout,"indgang TeoretiskEnergi NumeriskEnergi\n");
for(int x=0;x<n;x++){
double TeoretiskEnergi=pow(M_PI*(x+1),2); // Her udregnes den eksakte energi.
double NumeriskEnergi=gsl_vector_get(evalue,x); // indgang k i egenvaerdi-vektoren hentes
fprintf(stdout,"%i \t %g \t %g \n",x,TeoretiskEnergi,NumeriskEnergi);
}

// v)
printf("\n\n");
for(int k=0;k<3;k++){ // Udskriver værdier for 3 laveste egenfunktioner.
  printf("%g %g\n",0.0,0.0); // Ved brøndens start skal egenfunktionen være nul.
  for(int i=0;i<n;i++){
  if(gsl_matrix_get(evector,0,k)<0) printf("%g %g\n",(i+1.0)/(n+1),-1.0*gsl_matrix_get(evector,i,k));
  else printf("%g %g\n",(i+1.0)/(n+1),gsl_matrix_get(evector,i,k));
  }
  printf("%g %g\n",1.0,0.0); // Ved brøndens slutning skal egenfunktionen være nul.
  printf("\n\n");
  }
// if-statementet ovenfor sørger for at jeg får "pæne egenvektorer". En egenvektor er også en egenvektor, hvis man ganger hele vektoren med -1. Så for at få nogle "pæne" egenvektorer sørger if-statementet for at vælge den egenvektor, der har positive værdier ved brøndens start.


gsl_matrix_free(H); // free-pladsen


// vi)
printf("Overensstemmelsen mellem den teoretiske energi og den numeriske energi er bedst for højt antal grid points.\n");




return 0;

}
