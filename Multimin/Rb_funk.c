#include<math.h>
#include<gsl/gsl_integration.h>
#include<gsl/gsl_errno.h>

double Rb_funk(double x, double y){
	double f=pow(1-x,2)+100*pow(y-x*x,2);
	return f;
}

