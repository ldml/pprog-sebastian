#define ALG gsl_multimin_fminimizer_nmsimplex2
#include<stdio.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_multimin.h>

double Rb_funk(double x,double y);


double master (const gsl_vector * xlist, void * params){
	double x=gsl_vector_get(xlist,0);
	double y=gsl_vector_get(xlist,1);
	return Rb_funk(x,y);
}


struct exp_data {int n; double *t,*y,*e;};

double master2 (const gsl_vector *x, void *params) {
	double  A = gsl_vector_get(x,0);
	double  T = gsl_vector_get(x,1);
	double  B = gsl_vector_get(x,2);
	struct exp_data *p = (struct exp_data*) params;
	int     n = p->n;
	double *t = p->t;
	double *y = p->y;
	double *e = p->e;
	double sum=0;
	#define f(t) A*exp(-(t)/T) + B
	for(int i=0;i<n;i++) sum+= pow( (f(t[i]) - y[i] )/e[i] ,2);
	return sum;
}

int main(){
	size_t dim=2;
	gsl_multimin_function F;
	F.f=master;
	F.n=dim;
	F.params=NULL;

gsl_multimin_fminimizer * state =
gsl_multimin_fminimizer_alloc (ALG,dim);
gsl_vector *start = gsl_vector_alloc(dim);
gsl_vector *step = gsl_vector_alloc(dim);
gsl_vector_set(start,0,2); /* x_start */
gsl_vector_set(start,1,2); /* y_start */
gsl_vector_set_all(step,0.05);
gsl_multimin_fminimizer_set (state, &F, start, step);

int iter=0,status;
double acc=0.001;
do{
	iter++;
	int flag = gsl_multimin_fminimizer_iterate (state);
	if(flag!=0)break;
	status = gsl_multimin_test_size (state->size, acc);
	if (status == GSL_SUCCESS) fprintf (stderr,"converged\n");
	fprintf(stderr,
		"iter=%2i, x= %8f, y= %8f, Rb_funk= %8g, size= %8g\n",
		iter,
		gsl_vector_get(state->x,0),
		gsl_vector_get(state->x,1),
		state->fval,
		state->size);
}while(status == GSL_CONTINUE && iter < 99);
	
fprintf(stderr,"\nMinimum of the  Rosenbrock function is thus at x=%g, y=%g.\n",gsl_vector_get(state->x,0),gsl_vector_get(state->x,1));

gsl_vector_free(start);
gsl_vector_free(step);
gsl_multimin_fminimizer_free(state);




// Problem 2:

double t[]= {0.47,1.41,2.36,3.30,4.24,5.18,6.13,7.07,8.01,8.95};
double y[]= {5.49,4.08,3.54,2.61,2.09,1.91,1.55,1.47,1.45,1.25};
double e[]= {0.26,0.12,0.27,0.10,0.15,0.11,0.13,0.07,0.15,0.09};
int n = sizeof(t)/sizeof(t[0]);

fprintf (stderr,"\n\n");
fprintf (stderr,"Problem 2:\n");
fprintf (stderr,"\n\n");

fprintf(stderr,"t \t y \t e \n");
for(int i=0;i<n;i++){
fprintf(stderr,"%g \t %g \t %g\n",t[i],y[i],e[i]);
}
fprintf(stderr,"\n\n");

struct exp_data params;
	params.n=n;
	params.t=t;
	params.y=y;
	params.e=e;

size_t dim2=3;
	gsl_multimin_function F2;
	F2.f=master2;
	F2.n=dim2;
	F2.params=(void*)&params;

gsl_multimin_fminimizer * state2 =
gsl_multimin_fminimizer_alloc (ALG,dim2);
gsl_vector *start2 = gsl_vector_alloc(dim2);
gsl_vector *step2 = gsl_vector_alloc(dim2);
gsl_vector_set(start2,0,2); /* A_start */
gsl_vector_set(start2,1,2); /* T_start */
gsl_vector_set(start2,2,2); /* B_start */
gsl_vector_set_all(step2,0.1);
gsl_multimin_fminimizer_set (state2, &F2, start2, step2);

fprintf(stderr,"Least square fit data: \n\n\n");

int iter2=0,status2;
double acc2=0.001;
do{
	iter2++;
	int flag2 = gsl_multimin_fminimizer_iterate (state2);
	if(flag2!=0)break;
	status2 = gsl_multimin_test_size (state2->size, acc2);
	if (status2 == GSL_SUCCESS) fprintf (stderr,"converged\n");
	fprintf(stderr,
		"iter=%2i, A= %8f, T= %8f, B= %8f, F= %8g, size= %8g\n",
		iter2,
		gsl_vector_get(state2->x,0),
		gsl_vector_get(state2->x,1),
		gsl_vector_get(state2->x,2),
		state2->fval,
		state2->size);
}while(status2 == GSL_CONTINUE && iter2 < 99);
	
fprintf(stderr,"The least square fit values of A, T and B are now found.\n");
fprintf(stderr,"In the next index the value of the function for t between 0 and 9 is printed\n\n\n");

double A=gsl_vector_get(state->x,0);
double T=gsl_vector_get(state->x,1);
double B=gsl_vector_get(state->x,2);

fprintf(stderr,"t \t f(t)=A*exp(-t/T)+B\n");
for(double t=0; t<9; t+=0.05){
fprintf(stderr,"%g \t %g\n",t,A*exp(-t/T)+B);
}


gsl_vector_free(start2);
gsl_vector_free(step2);
gsl_multimin_fminimizer_free(state2);


return 0;
}
