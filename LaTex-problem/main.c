#include<stdio.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int diff_eq(double t, const double y[], double dydt[], void * params){
	dydt[0]=2*pow(M_PI,-0.5)*exp(-pow(t,2));
return GSL_SUCCESS;
}


double err_function(double x){
	gsl_odeiv2_system system;
	system.function = diff_eq;
	system.jacobian = NULL;
	system.dimension = 1;
	system.params = NULL;

double acc=1e-6,eps=1e-6,hstart=copysign(0.1,x);

gsl_odeiv2_driver* driver = gsl_odeiv2_driver_alloc_y_new(&system, gsl_odeiv2_step_rkf45, hstart, acc, eps);

double t=0,y[1]={0.0};

gsl_odeiv2_driver_apply(driver,&t,x,y);

gsl_odeiv2_driver_free(driver);

return y[0];
}



int main(int argc, char *argv[]){

double x1 = atof(argv[1]);
double x2 = atof(argv[2]);
double dx = atof(argv[3]);

for(double x=x1;x<=x2;x+=dx){ printf("%g \t %g \n",x,err_function(x));
}



return 0;
}


