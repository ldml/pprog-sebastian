#include <stdio.h>
#include <complex.h>
#include <math.h>

int main(void)
{
	double complex i,j,k,l,m,n;

	i = csqrt(-2);
	j=cpow(M_E,I);
	k=cpow(M_E,I*M_PI);
	l=cpow(I,M_E);
	m=tgamma(5);
	n=j1(0.5);

	printf("\nProblem 1:\n");
	printf("sqrt(-2) = %lg + %lg*i \n", creal(i),cimag(i));
	printf("e^i = %lg + %lg*i \n", creal(j),cimag(j));
	printf("e^(i*pi) = %lg + %lg*i \n", creal(k),cimag(k));
	printf("i^e %lg + %lg*i \n", creal(l),cimag(l));
	printf("Gamma(5) = %lg + %lg*i \n", creal(m),cimag(m));
	printf("J1(0.5) = %lg + %lg*i \n",creal(n),cimag(n));
	
	
	printf("\n\nProblem 2:\n");
	float x=0.1111111111111111111111111111111111;
	double y=0.1111111111111111111111111111111111;
	long double z=0.1111111111111111111111111111111111L;
	printf("Antal float-tal = \t %.25g \nAntal double-tal = \t %.25lg \nAntal long double-tal =  %.25Lg \n",x,y,z);


	return 0;
}

