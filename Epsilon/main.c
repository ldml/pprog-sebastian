#include<stdio.h>
#include<math.h>
#include<limits.h>
#include<float.h>

int equal(double a, double b, double tau, double epsilon); // function declaration

int main(void)
{
	printf("\nProblem 1i:\n");
	int i=1; while(i+1>i) {i++;}

	printf("while: i=%i, INT_MAX=%i\n",i,INT_MAX); //print i og INT_MAX, så man kan se deres værdier


// Nu med for-loop:

	for(i=1; i+1>i; i++){}

	printf("for: i=%i\n",i);

// Nu med do while-loop:


	i=1;
	do {i++;
	} while(i+1>i);

	printf("do while: i=%i\n",i);




	printf("\n\nProblem 1ii:\n");

	i=1; 
	while(i-1<i) {i--;}

	printf("while: i=%i, INT_MIN=%i\n",i,INT_MIN); //print i og INT_MAX, så man kan se deres værdier


// Nu med for-loop:

	i=1;
	for(i=1; i-1<i; i--){}

	printf("for: i=%i\n",i);

// Nu med do while-loop:

	i=1;
	do {i--;
	} while(i-1<i);

	printf("do while: i=%i\n",i);



printf("\n\nProblem 1ii:\n");

double x=1; while(1+x!=1){x/=2;} x*=2; printf("double while=%g, DBL_EPSILON=%g\n",x,DBL_EPSILON);

for(x=1; 1+x!=1; x/=2){} x*=2; printf("double for=%g\n",x);

x=1; do{x/=2;} while(1+x!=1); x*=2; printf("double do while=%g\n",x); 


float y=1; while(1+y!=1){y/=2;} y*=2; printf("float while=%g, FLT_EPSILON=%g\n",y,FLT_EPSILON);

for(y=1; 1+y!=1; y/=2){} y*=2; printf("float for=%g\n",y);

y=1; do{y/=2;} while(1+y!=1); y*=2; printf("float do while=%g\n",y); 


long double z=1; while(1+z!=1){z/=2;} z*=2; printf("long double while=%Lg, LDBL_EPSILON=%Lg\n",z,LDBL_EPSILON);

for(z=1; 1+z!=1; z/=2){} z*=2; printf("long double for=%Lg\n",z);

z=1; do{z/=2;} while(1+z!=1); z*=2; printf("long double do while=%Lg\n",z); 



printf("\n\nProblem 2:\n");


int max=INT_MAX/3;

int n;
float sum_up_float, sum_down_float;

for(n=1; n<=max; n++){sum_up_float+=1.0f/n;}
printf("sum_up_float=%g\n",sum_up_float);

for(n=max; n>=1; n--){sum_down_float+=1.0f/n;}
printf("sum_down_float=%g\n",sum_down_float);


printf("\nsum_down_float er større end sum_up_float. I sum_up_float starter summen med de største tal. Der bliver altså hele tiden lagt mindre og mindre tal til. På et tidspunkt lægges værdierne ikke til længere fordi man lægger et meget lille tal til et meget større tal. sum_down_float starter summen med de mindste tal. Altså lægges små tal til små tal. Her er 'ignoreres' de små tal altså ikke, og den samlede sum bliver derfor større end for sum_up_float\n");

printf("\nNår max bliver større bliver forskellen mellem sum_down_float og sum_up_float større. sum(1/n) for n gående mod uendelig konvergerer ikke, men går også mod uendelig.\n\n");


double sum_up_double, sum_down_double;

for(n=1; n<=max; n++){sum_up_double+=1.0f/n;}
printf("sum_up_float=%g\n",sum_up_double);

for(n=max; n>=1; n--){sum_down_double+=1.0f/n;}
printf("sum_down_float=%g\n",sum_down_double);

printf("For double giver de to summer det samme. Double er mere præcis, når der skal lægges små tal til. Det så vi også i problem 1, da FLT_EPSILON > DBL_EPSILON. Derfor giver de to summer de samme, og summen for double er større end summerne for float.\n");




printf("\n\nProblem 3:\n");
printf("Her testes funktionen equal(a,b,tau,epsilon):\n");
printf("equal(-1,2,2,1)=%i. Det burde give 0 \nequal(-1,2,2,3)=%i. Det burde give 1 \nequal(-1,2,5,1)=%i. Det burde give 1 \nequal(-1,2,5,5)=%i. Det burde give 1 \nequal(2,-1,5,5)=%i. Det burde give 1 \n",equal(-1,2,2,1),equal(-1,2,2,3),equal(-1,2,5,1),equal(-1,2,5,5),equal(2,-1,5,5));


return 0;
}
