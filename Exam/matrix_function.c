#include<gsl/gsl_eigen.h>


double matrix_function(int n){
gsl_matrix *M = gsl_matrix_calloc(n, n); // alloc. plads til nxn matrix.

for(int y=0;y<=n-1;y++){

for(int x=0;x<=n-1;x++){
gsl_matrix_set(M, x, y, 1);
}

}

gsl_eigen_symmv_workspace *w = gsl_eigen_symmv_alloc(n); // Her alloc. plads til at udregne egenværdierne og egenvektorene.

gsl_vector* evalue = gsl_vector_alloc(n); // Her alloc. plads til en vektor med n indgange, hvor egenvaerdierne indsættes.
gsl_matrix* evector = gsl_matrix_calloc(n,n); // Her alloc. plads til egenvektorene.
gsl_eigen_symmv(M,evalue,evector,w); // Her udregnes egenværdierne og egenvektorene og gemmes i hhv. evalue og evector.

gsl_eigen_symmv_sort(evalue,evector,GSL_EIGEN_SORT_VAL_DESC); // Her sorteres egenværdierne og egenvektorene med stoerst vaerdier foerst.


return gsl_vector_get(evalue, 0);

gsl_matrix_free(M); // free-pladsen
gsl_eigen_symmv_free(w);
gsl_vector_free(evalue);
gsl_matrix_free(evector);
}
