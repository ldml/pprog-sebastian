#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_multiroots.h>
#define ALG gsl_multiroot_fsolver_hybrids


// Problem 1:

int master(const gsl_vector * x, void *params, gsl_vector * f)
{

  const double x0 = gsl_vector_get (x, 0);
  const double x1 = gsl_vector_get (x, 1);

  const double y0 = -2*(1-x0)-2*100*x0*2*(x1-x0*x0);
  const double y1 = 100*2*(x1-x0*x0);

  gsl_vector_set (f, 0, y0);
  gsl_vector_set (f, 1, y1);

  return GSL_SUCCESS;
}

// Problem 2:
double Hydrogen_funk(double eps, double r);

int master2(const gsl_vector * x, void * params, gsl_vector * f){
	double eps = gsl_vector_get(x,0);
	double rmax = *(double*)params;
	gsl_vector_set(f,0,Hydrogen_funk(eps,rmax));
	return GSL_SUCCESS;
}


int main(){
// Problem 1:
const int dim = 2;

gsl_multiroot_function M;
M.f = master;
M.n = dim;
M.params =NULL;

gsl_multiroot_fsolver *s = gsl_multiroot_fsolver_alloc (ALG, dim);
gsl_vector * x = gsl_vector_alloc(dim);
gsl_vector_set(x,0,2);
gsl_vector_set(x,1,2);
gsl_multiroot_fsolver_set (s, &M, x);

int iter=0,status;
const double acc=1e-6;
do{
	iter++;
	int flag=gsl_multiroot_fsolver_iterate(s);
	if(flag!=0)break;
	status = gsl_multiroot_test_residual (s->f, acc);
  	fprintf (stderr,"iter = %3i x = %5f y= %5f "
          "f'x = %8g f'y = %8g\n",
          iter,
          gsl_vector_get (s->x, 0), 
          gsl_vector_get (s->x, 1),
          gsl_vector_get (s->f, 0), 
          gsl_vector_get (s->f, 1));
}while(status == GSL_CONTINUE && iter<999);

fprintf(stderr,"We can see from the shooting method that the roots of the derivative of the R.B.-function is x=%g and y=%g. So the minimum of the R.B.-function is at x=%g and y=%g.\n",gsl_vector_get (s->x, 0),gsl_vector_get (s->x, 1),gsl_vector_get (s->x, 0),gsl_vector_get (s->x, 1));

gsl_vector_free(x);
gsl_multiroot_fsolver_free (s);

// Problem 2:
fprintf(stderr,"\n\n\n");
const int dim2 = 1;
const double rmax = 8;
gsl_multiroot_function M2;
M2.f = master2;
M2.n = dim2;
M2.params = (void*)&rmax;

gsl_multiroot_fsolver *s2 = gsl_multiroot_fsolver_alloc (ALG, dim2);
gsl_vector * x2 = gsl_vector_alloc(dim2);
gsl_vector_set(x2,0,-1);
gsl_multiroot_fsolver_set (s2, &M2, x2);

int iter2=0,status2;
const double acc2=1e-3;
do{
	iter2++;
	int flag2=gsl_multiroot_fsolver_iterate(s2);
	if(flag2!=0)break;
	status2 = gsl_multiroot_test_residual (s2->f, acc2);
	fprintf(stderr
		,"iter= %3i, Eps= %8g, Hydrogen_funk(Eps,r=%g)= %g\n"
		,iter2
		,gsl_vector_get(s2->x,0)
		,rmax
		,gsl_vector_get(s2->f,0)
		);
}while(status2 == GSL_CONTINUE && iter2<999);

fprintf(stderr,"\n\n\nSo the converged energy is %8g. Values of f(eps,r) is printed below with the converged energy. f(eps,r) is printed in the range 0<r<10. This data is plotted in plot.pdf along with the exact solution.\n\n\n ",gsl_vector_get(s2->x,0));

fprintf(stderr,"r \t f_num\n");
for(double ri=0.01;ri<10;ri+=0.01){
fprintf(stderr,"%4g \t %8g\n",ri,Hydrogen_funk(gsl_vector_get(s2->x,0),ri));
}


gsl_vector_free(x2);
gsl_multiroot_fsolver_free (s2);



return 0;
}
