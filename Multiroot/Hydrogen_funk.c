#include<stdio.h>
#include<assert.h>
#include<math.h>
#include<gsl/gsl_odeiv2.h>
#include<gsl/gsl_errno.h>

int diff_equation // -(1/2)f''-(1/r)f = eps f
(double r, const double y[], double dydr[], void * params)
{ //y0=f & y1=f'
	double eps = *(double*)params;
	dydr[0]=y[1]; // y0'=y1
	dydr[1]=-2*(1/r+eps)*y[0]; // f''=-2*(eps + 1/r) f. So y1'=-2*(eps+1/r)*y0
return GSL_SUCCESS;
}

double Hydrogen_funk(double eps, double r){
	assert(eps<0); // Needs to be a bound state
	assert(r>=0); // r less than 0 does not make physical sense.
	const double rmin=1e-3;
	if(r<rmin)return r-pow(r,2); // f(r --> 0)=y0(r --> 0)=r-r^2

	gsl_odeiv2_system sys;
	sys.function = diff_equation;
	sys.jacobian = NULL;
	sys.dimension = 2;
	sys.params = (void*)&eps;

	double acc=1e-6,eps_acc=1e-6,hstart=0.01;
	gsl_odeiv2_driver *driver=gsl_odeiv2_driver_alloc_y_new
		(&sys,gsl_odeiv2_step_rkf45,hstart,acc,eps_acc);

	double r_run=rmin,y[2]={r_run-pow(r_run,2),1-2*r_run};
	gsl_odeiv2_driver_apply(driver,&r_run,r,y);

	gsl_odeiv2_driver_free(driver);
	return y[0];
}
